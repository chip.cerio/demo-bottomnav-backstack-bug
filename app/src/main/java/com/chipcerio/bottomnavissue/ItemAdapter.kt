package com.chipcerio.bottomnavissue

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ItemAdapter(
    private val listener: OnItemClickListener?
) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {
    
    override fun getItemCount(): Int = Cheese.LIST.size
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val root = inflater.inflate(android.R.layout.simple_list_item_1, parent, false)
        return ViewHolder(root)
    }
    
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(Cheese.LIST[position], listener)
    }
    
    interface OnItemClickListener {
        fun onItemClick(item: String)
    }
    
    inner class ViewHolder(private val container: View) : RecyclerView.ViewHolder(container) {
        
        private val title: TextView = container.findViewById(android.R.id.text1)
        
        fun bind(item: String, listener: OnItemClickListener?) {
            title.text = item
            container.setOnClickListener { listener?.onItemClick(item) }
        }
    }
}