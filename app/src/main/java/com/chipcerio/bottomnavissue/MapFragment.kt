package com.chipcerio.bottomnavissue

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chipcerio.bottomnavissue.MapFragmentDirections.actionMapToMapDetails

class MapFragment : Fragment(), ItemAdapter.OnItemClickListener {
    
    private lateinit var recyclerView: RecyclerView
    
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_map, container, false)
    
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = view.findViewById(R.id.recycler_map)
        
        recyclerView.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(requireContext())
            adapter = ItemAdapter(this@MapFragment)
        }
    }
    
    override fun onItemClick(item: String) {
        val action = actionMapToMapDetails().setMapItem(item)
        findNavController().navigate(action)
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("MapFragment", "onCreate")
    }
    
    override fun onDestroy() {
        super.onDestroy()
        Log.d("MapFragment", "onDestroy")
    }
}